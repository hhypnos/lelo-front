import React from 'react';
import DonationHeader from './DonationHeader/DonationHeader';
import './DonationPage.css'

const DonationPage = () => {
    return (
        <div style={{ marginTop: '36px', display:'flex', justifyContent:'center'}}>
      <div className="donationView">
          <DonationHeader />
      </div>
      </div>
    );
};

export default DonationPage;
