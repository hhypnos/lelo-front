import React from 'react';
import HeaderComponent from '../../components/Header/HeaderComponent';
import Slider from '../../components/Slider/Slider';
import CardComponent from '../../components/card/CardComponent';
import Donation from '../../components/Donation/Donation';

const HomePage = () => {
    return(
        <div>
        {/* <HeaderComponent />
        <Slider />
        <CardComponent /> */}
        <Donation />
        </div>
    )
}

export default HomePage