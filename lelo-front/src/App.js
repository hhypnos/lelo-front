import './App.css';
import { Switch, Route } from "react-router-dom";
import HomePage from './pages/HomePage/HomePage';
import DonationPage from './pages/DonationPage/DonationPage';

function App() {
  return (
 <>
   <Switch>
     <Route  exact path="/" component={HomePage}/>
      <Route   path="/donation" component={DonationPage}/>
   </Switch>
 </>
  );
}

export default App;
